#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

#define N 1000 // number of steps used in the numerical integration

double g_Suma = 0.0;

int    g_iThreads;            // number of threads to use
double g_PiEstimate =0;       // global variable for the estimate 
pthread_mutex_t gLock;        // global object for mutex code

//Prints the usage of the program if needed
void print_usage ()
{
    printf ("Usage: $ ./pi -t threads \n");
    printf ("  -t threads : the number of threads to use\n");
    exit (0);
}

//Function to use in the quadrature for estimate pi
double function(double x){ return 1/(1+x*x); }

//Task for the thread, evaluate and sum the function 
void *evaluateAndSum(void *arguments)
{
    int l_iBeginIndex = *((int *)arguments);
    int l_iEndIndex = *((int *)arguments + 1);
    double l_dStep = 1/((double) N); // Step of numerical integration method
    
    double l_dSum = 0;           // variable for acumulate the local sum
    
    for(int i=l_iBeginIndex;i<l_iEndIndex;i++)
    {
        l_dSum+=function(((double) i) * l_dStep);
    }

    pthread_mutex_lock(&gLock);  // lock the global variable for 
    g_Suma+=l_dSum;              // acumulate the sums of each thread
    pthread_mutex_unlock(&gLock);// unlock the variable
    pthread_exit(NULL);          // finish the thread
}
////////////////////////////////////////
//               Main                 //
////////////////////////////////////////
int main(int argc, char * argv []) {
    ////////////////////////////////////////
    // Process the command line arguments //
    ////////////////////////////////////////
    if ( argc == 3 )
    { 
        if (strcmp ("-t", argv [1]) == 0) { g_iThreads = atoi (argv [2]);} 
        else { print_usage (); }
    } else{ print_usage (); }
    ////////////////////////////////////////
    //     Run the paralell algoritm      //
    ////////////////////////////////////////
    clock_t tic = clock();
    pthread_t threads[g_iThreads];    // array of threads to use
    pthread_mutex_init(&gLock, NULL); // init the mutex object
    int thread_args[g_iThreads*2];    // array for threads arguments
    ////////////////////////////////////////
    int l_iResult_code;              // code for check errors of threads

    int l_iIndex = N/g_iThreads;     // step of sum by thread
    int jj =1;
    
    for (int i = 0; i < g_iThreads; i++) 
    {
        thread_args[2*i] = jj;            // the index to start the sum
        thread_args[2*i+1] = jj+l_iIndex; // the index to finish the sum
        jj+=l_iIndex;
        if(i+1==g_iThreads){thread_args[2*g_iThreads-1]=N;}
        
        l_iResult_code = pthread_create(&threads[i], NULL, evaluateAndSum, &thread_args[2*i]);
        assert(!l_iResult_code);
    }

    //wait for each thread to complete, they acumulate the results in g_Suma
    for (int i = 0; i < g_iThreads; i++) 
    {
        l_iResult_code = pthread_join(threads[i], NULL);
        assert(!l_iResult_code);  // check if error
    }
    // finish the computation
    double l_dPiThreads = 4.0*(function(0.0)+function(1.0)+2.0*g_Suma)/(2.0*N);
    clock_t toc = clock();
    pthread_mutex_destroy(&gLock);
    
    // print results
    printf("Threads used:                        %d\n", g_iThreads);
    printf("Time Elapsed:                        %f seconds\n", (double)(toc - tic) / CLOCKS_PER_SEC);
    
    printf("Estimate trapezoidal rule (threads): %1.10lf\n", l_dPiThreads);

    return 0;
}
