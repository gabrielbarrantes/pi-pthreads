# Pi estimate using pthreads

A program for estimate the constant pi with at least five correct decimals using
pthreads on a share variable.

The program takes as input the number of threads to use.  

## Instructions for use:

$ ./pi -t < # >

Where -t is the number of threads to use.

### Example of use:
    $ ./pi -t 5

### Output:
    Threads used:                        5
    Time Elapsed:                        0.001647 seconds
    Estimate trapezoidal rule (threads): 3.1415924869

